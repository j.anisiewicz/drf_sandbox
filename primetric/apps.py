from django.apps import AppConfig


class PrimetricConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'primetric'
