from django.db import models
from django.db.models import manager

# Create your models here.

class Department(models.Model):
    name = models.CharField(max_length=200)
    manager = models.ForeignKey(
        'primetric.Person', 
        on_delete=models.SET_NULL, 
        null=True, 
        default=True, 
        blank=True
    )


class Person(models.Model):
    name = models.CharField(max_length=200)

    department = models.ForeignKey(
        'primetric.Department', 
        on_delete=models.SET_NULL, 
        null=True, 
        default=True, 
        blank=True
    )



class Worklog(models.Model):
    person = models.ForeignKey('primetric.Person', on_delete=models.CASCADE)

    work = models.PositiveBigIntegerField(default=0)
    cost = models.DecimalField(
        default=0,
        decimal_places=2,
        max_digits=20,
    )
    income = models.DecimalField(
        default=0,
        decimal_places=2,
        max_digits=20,
    )
