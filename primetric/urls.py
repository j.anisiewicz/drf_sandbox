from django.urls import path
from django.urls.conf import include
from rest_framework.routers import DefaultRouter

from .views import PersonViewSet

person_router = DefaultRouter()
person_router.register('', PersonViewSet, 'people')

urlpatterns = [
    path('people/',
         include(person_router.urls)),
]
